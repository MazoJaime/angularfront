import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './components/login.component';
import { RegisterComponent } from './components/register.component';
import { HomeComponent } from './components/home.component';
import { UserEditComponent } from './components/user.edit.component';
import { TaskNewComponent } from './components/task.new.component';
import { TaskDetailComponent } from './components/task.detail.component';
import { TaskEditComponent } from './components/task.edit.component';

const appRoutes: Routes = [
    {path: '', component: HomeComponent },
    {path: 'index', component: HomeComponent },
    {path: 'index/:page', component: HomeComponent },
    {path: 'login', component: LoginComponent },
    {path: 'login/:id', component: LoginComponent },
    {path: 'register', component: RegisterComponent },
    {path: 'user-edit', component: UserEditComponent },
    {path: 'task-new', component: TaskNewComponent },
    {path: 'task-edit/:id', component: TaskEditComponent },
    {path: 'task/:id', component: TaskDetailComponent },
    {path: '**', component: LoginComponent }

];

export const appRoutingProviders: any[] = [];
export const Routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);