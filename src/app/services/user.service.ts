import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import "rxjs/add/operator/map";
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import { GLOBAL } from './global';

@Injectable()
export class UserService{
    public url: string;
    public identity;
    public token;

    constructor(private _http: HttpClient){
    this.url = GLOBAL.url;
    }

    singup(user_to_login): Observable<any>{
        let json=JSON.stringify(user_to_login);
        let params = "json="+json;
        let headers = new HttpHeaders({'content-Type':'application/x-www-form-urlencoded'});
        //alert(json);
        return this._http.post(this.url+'/login', params, {headers :headers});
               
    }

    getIdentity(){
        let identity =JSON.parse(localStorage.getItem('identity'));
        if(identity != "undefined"){
            this.identity = identity;
        }else{
            this.identity = null;
        }
        return this.identity;
    }
        getToken(){
        let token =JSON.parse(localStorage.getItem('token'));
        if(token != "undefined"){
            this.token = token;
        }else{
            this.token = null;
        }
        return this.token;
    }

        register(user_to_register): Observable<any>{
        let json = JSON.stringify(user_to_register);
        let params = "json="+json;
        let headers = new HttpHeaders({'content-Type':'application/x-www-form-urlencoded'});
        
        return this._http.post(this.url+'/user/new', params, {headers :headers});
    }

        updateUser(user_to_register): Observable<any>{
        let json = JSON.stringify(user_to_register);
        let params = "json=" + json + "&authorization=" + this.getToken();
        let headers = new HttpHeaders({'content-Type':'application/x-www-form-urlencoded'});
        
        return this._http.post(this.url+'/user/edit', params, {headers :headers});
    }

        newTask(task): Observable<any>{
        let json = JSON.stringify(task);
        let params = "json=" + json + "&authorization=" + this.getToken();
        let headers = new HttpHeaders({'content-Type':'application/x-www-form-urlencoded'});
        
        return this._http.post(this.url+'/task/new', params, {headers :headers});
    }
}

