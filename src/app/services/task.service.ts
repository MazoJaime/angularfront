import {Injectable} from '@angular/core';
import "rxjs/add/operator/map";
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from "@angular/common/http";
//hasta aqui es requerido para un servicio.
import {GLOBAL} from './global';

@Injectable()
export class TaskService {
    public url: string;

    constructor(private _http: HttpClient) {
        this.url = GLOBAL.url;
    }

    create(token, task): Observable<any> {

        let json = JSON.stringify(task);
        let params = "json=" + json + "&authorization=" + token;
        let headers = new HttpHeaders({'content-Type': 'application/x-www-form-urlencoded'});

        return this._http.post(this.url + '/task/new', params, {headers: headers});

    }

    getTasks(token, page = null): Observable<any> {

        let params = "authorization=" + token;
        let headers = new HttpHeaders({'content-Type': 'application/x-www-form-urlencoded'});

        if (page === null) {
            page = 1;
        }

        return this._http.post(this.url + '/task/list?page=' + page, params, {headers: headers});
    }

    getTask(token, id): Observable<any> {

        let params = "authorization=" + token;
        let headers = new HttpHeaders({'content-Type': 'application/x-www-form-urlencoded'});

        return this._http.post(this.url + '/task/detail/' + id, params, {headers: headers});
    }

    update(token, task, id): Observable<any> {
        let json = JSON.stringify(task);
        let params = "json=" + json + "&authorization=" + token;
        let headers = new HttpHeaders({'content-Type': 'application/x-www-form-urlencoded'});

        return this._http.post(this.url + '/task/edit/' + id, params, {headers: headers});
    }

    search(token, search = null, filter = null, order = null): Observable<any> {
        let params = "authorization=" + token + "&filter=" + filter + "&order=" + order;
        let headers = new HttpHeaders({'content-Type': 'application/x-www-form-urlencoded'});
        let url: string;
        //condicionamos la url si tiene o no texto como en el php
        // para que busque como corresponde
        if (search == null) {
            url = this.url + '/task/search';
        } else {
            url = this.url + '/task/search/' + search;
        }

        return this._http.post(url, params, {headers: headers});

    }

    deleteTask(token, id): Observable<any> {

        let params = "authorization=" + token;
        let headers = new HttpHeaders({'content-Type': 'application/x-www-form-urlencoded'});

        return this._http.post(this.url + '/task/remove/' + id, params, {headers: headers});

    }
}
