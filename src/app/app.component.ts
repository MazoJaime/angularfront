import { Component, OnInit } from '@angular/core';
import { UserService } from './services/user.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [UserService]    
})
export class AppComponent {
 public title = 'app';
 public identity;
public token;


constructor(
private _UserService: UserService,
){
this.identity = this._UserService.getIdentity();
this.token = this._UserService.getToken();
}

ngOnInit(){

    }
}
