import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { User } from '../models/user';
import { UserService } from '../services/user.service';

@Component({
    selector: 'register',
    templateUrl: '../views/register.html',
    providers: [UserService]
})
export class RegisterComponent implements OnInit{
    public title: string;
    public _user: User;
    public status;

    constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _UserService: UserService
    ){
        this.title = 'Registro';
        this._user = new User(1, "User", "Su nombre", "", "", "");
    }

    ngOnInit(){
}

    onSubmit(){
    this._UserService.register(this._user).subscribe(
        response=>{
        this.status = response.status;
        if(response.status != 'succes'){
            this.status='error'
            }
        },
        error=>{
            console.log(<any>error);
            }
        );
    }

}
