import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserService } from '../services/user.service';
import { TaskService } from '../services/task.service';
import { Task } from '../models/task';
import { generateDatePipe } from '../pipes/generate.date.pipe';
@Component({
    selector: 'task-detail',
    templateUrl: '../views/task_detail.html',
    providers: [UserService, TaskService]
})

export class TaskDetailComponent implements OnInit{
    public title_page: string;
    public token;
    public identity;
    public task: Task;
    public loading;
    

    constructor(
    private _userService: UserService,
    private _router: Router,
    private _route: ActivatedRoute,
    private _taskService: TaskService
    ){
    this.title_page = "Modificar tarea";
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
        
    }

    ngOnInit(){       
        if(this.identity && this.identity.sub){
            this.getTask();
   
        }else{
            this._router.navigate(['/login']);
        
        
        }
    }

    getTask(){
    this.loading='show';
        this._route.params.forEach((params: Params) => {
            let id= +params['id'];
            console.log('prueba desde task detail');
            this._taskService.getTask(this.token,id).subscribe(
                response =>{
                    var mensaje = response.mensaje;
                    var nologed = "autenticacion no valida";
                    if(response.status ==='succes'){
                        if(response.data.user.id == this.identity.sub || "admin" == this.identity.role){
                            this.task = response.data;
                            this.loading='hide';
                        }
                    }else{

                    if(response.status == 'error') {
                        let errorlog = (new String(mensaje).valueOf() === new String(nologed).valueOf());
                        console.log(errorlog);
                            if(errorlog == true){
                            this._router.navigate(['/login/1']);
                            }else{
                            
                            }
                        }else{
                        this._router.navigate(['/']);
                        }
                    
                    }
                },
                error => {
                console.log(<any>error);
                }
            );
        });
    } 

    deleteTask(id){


    this._taskService.deleteTask(this.token,id).subscribe(
        response => {
            if(response.status="success"){
                this._router.navigate(['/']);
            }else{
            alert('Ha ocurrido un error, intentelo de nuevo.');
            }
        },
        error => {
            console.log(<any>error);
        }
    );


    }

}