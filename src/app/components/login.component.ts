import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserService } from '../services/user.service';

@Component({
    selector: 'login',
    templateUrl: '../views/login.html',
    providers: [UserService, ]
})
export class LoginComponent implements OnInit{
    public title: string;
    public user;
    public identity;
    public token;

    constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _UserService: UserService
    ){
        this.title = 'Tareas';
        this.user = {
        "email": "",
        "password":"",
        "getHash":"true"
        };

    }

    ngOnInit(){
    this.logout();
    this.redirectIfIdentity();
    }

    logout(){
    this._route.params.forEach((params: Params)=>{
        let logout = +params['id'];
        if(logout == 1){
            
            localStorage.removeItem('identity');
            localStorage.removeItem('token');
            this.identity = null;
            this.token = null;

            window.location.href= '/login';
        }
    });
    }
    //redireccion desde login en caso de estar logueado (bloquea /login en caso de estar con sesion iniciada)
    redirectIfIdentity(){
        let identity =this._UserService.getIdentity();
        if(identity != null && identity.sub){
        this._router.navigate(["/"]);
    }
    }

    onSubmit(){

    this._UserService.singup(this.user).subscribe(
        response => {
        this.identity = response;

            if(this.identity.length <=1){
            }{
                if(!this.identity.status){

                    localStorage.setItem('identity', JSON.stringify(this.identity));
                    
                    //obtenemos el token seteamos null Hash para que regrese token encriptado
                    //
                    this.user.getHash= null;
                    this._UserService.singup(this.user).subscribe(
                            response => {
                             this.token = response;
            
                                    if(this.identity.length <=1){
                                 }{
                                        if(!this.identity.status){
                                        //fin del login correcto, guarda token y deriva a pagina inicial
                                        localStorage.setItem('token', JSON.stringify(this.token));
                                        window.location.href = '/';
                }
            }
        },
        error =>{
            console.log(<any>error);
        }
    );


                }
            }
        },
        error =>{
            console.log(<any>error);
        }
    );
    }
    
}
